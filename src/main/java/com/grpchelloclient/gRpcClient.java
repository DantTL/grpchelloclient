package com.grpchelloclient;

import com.grpchelloclient.proto.HelloRequest;
import com.grpchelloclient.proto.HelloResponse;
import com.grpchelloclient.proto.HelloServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class gRpcClient {
    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder
                .forAddress("localhost",6565)
                .usePlaintext()
                .build();

        HelloServiceGrpc.HelloServiceBlockingStub stub = HelloServiceGrpc.newBlockingStub(channel);

        HelloResponse helloResponse = stub.hello(
                HelloRequest.newBuilder()
                        .setFirstName("Daniel")
                        .setLastName("Torres")
                        .build()
        );

        channel.shutdown();
        System.out.println(helloResponse.getGreeting());
    }
}
